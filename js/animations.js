$(function() {

/*=====  ANIMATIONS PAGE D'ACCUEIL  ======*/

	function gotopagetwo(){
		window.location.href = "etape1.html";
	};

	// Animation du bateau
	var tl_boat = new TimelineMax({repeat: -1});

	tl_boat.fromTo('.bateau',4, {x:'3%', y:0, rotation: -2}, {x:'-3%', y:0, rotation: 2})
	.fromTo('.bateau',4, {x:'-3%', y:0, rotation: 2}, {x:'3%', y:0, rotation: -2});

	// Animation du bateau qui quitte la scene
	var tl_boatMove = new TimelineMax({repeat: 0, onComplete: gotopagetwo, paused: true});
	tl_boatMove.to('.bateau', 2, {left: '120%'});

	$(".btn_vert").click(function(event){
		event.preventDefault();
	    tl_boatMove.play();
	});

	// Animation du btn_vert
	var tl_btn = new TimelineMax({repeat: 2, repeatDelay:2.5, onComplete: function(){tl_boatMove.play();
	}});
	tl_btn.fromTo('#scene_home .btn_vert', 1.5, {scaleX:0, scaleY:0}, {scaleX:1, scaleY:1, ease: Elastic. easeOut.config( 1, 0.5)});

	// Animation des vagues
	// var tl_waves = new TimelineMax({repeat: -1});
	// tl_waves.fromTo('.vague', 4, {x:0, y:0, z:'8%'}, {x:0, y:0, z:'8%' });
});