/*=====  ANIMATIONS ECRAN 2  ======*/
$(function() {

	function gotopagethree(){
		window.location.href = "etape3.html";
	};

	$('.msg').click(function(){
		var tl_boatMove = new TimelineMax({onComplete: gotopagethree});
		tl_boatMove.to('.bateau_etape2', 2, {left: '120%'});
	})

	var tl_caisse = new TimelineMax({repeat: -1});

	tl_caisse.fromTo('#fil_droit', 5, {scaleY: 1.1}, {scaleY: 2}, 0.1)
	.to('#caisse', 5, {y: 150}, 0.1)
	.addLabel('up')
	.fromTo('#fil_droit', 5, {scaleY: 2}, {scaleY: 1.1}, 'up')
	.to('#caisse', 5, {y: 0}, 'up');

	// Animation du bateau qui entre en scene
	var tl_boatInSc2 = new TimelineMax({repeat: 0});

	tl_boatInSc2.fromTo('.bateau_etape2', 4, {left:'-100%'}, {left:'5%'});

	// Animation du bateau qui quitte la scene
	var tl_boatMove = new TimelineMax({repeat: 0, onComplete: gotopagethree, paused: true});
	tl_boatMove.to('.bateau_etape2', 2, {left: '120%'});

	// Animation btn_suite
	var tl_btn_suiteSc2 = new TimelineMax({repeat: 0});
	tl_btn_suiteSc2.fromTo('#scene_ecran2 .msg', 1, {top: "-200%"}, {top: "-85%"}, 2.5)
	.add(tl_boatMove.play(), 8.5);

	// Animation btn_suite
	// var tl_btn_suiteSc2 = new TimelineMax();
	// tl_btn_suiteSc2.fromTo('#scene_ecran2 .msg', 1, {top: "-200%"}, {top: "-75%"}, 3);
});