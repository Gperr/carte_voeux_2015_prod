/*=====  ANIMATIONS ECRAN 3  ======*/
$(function() {
	function gotopagefour(){
		window.location.href = "etape4.html";
	};

	var tl_rayon = new TimelineMax({repeat: -1});

	tl_rayon.fromTo('.rayon', 3, {rotation: "0deg"}, {rotation: "-4deg"})
	.fromTo('.rayon', 3, {rotation: "-4deg"}, {rotation: "0deg"});

	// Animation du bateau qui entre en scene
	var tl_boatInSc3 = new TimelineMax({repeat: 0});

	tl_boatInSc3.fromTo('.bateau_etape3', 4, {left:'-100%'}, {left:'5%'});

	// Animation du bateau qui quitte la scene
	var tl_boatMove = new TimelineMax({repeat: 0, onComplete: gotopagefour, paused: true});
	tl_boatMove.to('.bateau_etape3', 2, {left: '120%'});

	$('.msg').click(function(){
		var tl_boatMove = new TimelineMax({onComplete: gotopagefour});
		tl_boatMove.to('.bateau_etape3', 2, {left: '120%'});
	})

	// Animation btn_suite
	var tl_btn_suiteSc3 = new TimelineMax({repeat: 0});
	tl_btn_suiteSc3.fromTo('#scene_ecran3 .msg', 1, {top: "-200%"}, {top: "-85%"}, 2.5)
	.add(tl_boatMove.play(), 8.5);

	// Animation bateau avant/arriere
	var tl_boatSc3 = new TimelineMax({repeat: -1});
	tl_boatSc3.fromTo('.bateau_etape3',4, {x:'3%', rotation: -1}, {x:'-3%', rotation: 1})
	.fromTo('.bateau_etape3',4, {x:'-3%', rotation: 1}, {x:'3%', rotation: -1});
});