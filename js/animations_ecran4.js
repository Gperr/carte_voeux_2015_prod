/*=====  ANIMATIONS ECRAN 4  ======*/
$(function() {

	// Animation du bateau
	var tl_boat = new TimelineMax({repeat: -1});

	tl_boat.fromTo('.bateau',4, {x:'3%', y:0, rotation: -2}, {x:'-3%', y:0, rotation: 2})
	.fromTo('.bateau',4, {x:'-3%', y:0, rotation: 2}, {x:'3%', y:0, rotation: -2});

	// Animation du bateau qui entre en scene
	var tl_boatInSc4 = new TimelineMax({repeat: 0});

	tl_boatInSc4.fromTo('.bateau_etape4', 4, {left:'-100%'}, {left:'5%'});

	// Animation btn_suite
	var tl_btn_suiteSc4 = new TimelineMax();
	tl_btn_suiteSc4.fromTo('#scene_ecran4 .msg', 1, {top: "-200%"}, {top: "-75%"}, 3);

	// Animation bateau avant/arriere
	var tl_boatSc4 = new TimelineMax({repeat: -1});
	tl_boatSc4.fromTo('.bateau_etape4',4, {x:'3%', rotation: -1}, {x:'-3%', rotation: 1})
	.fromTo('.bateau_etape4',4, {x:'-3%', rotation: 1}, {x:'3%', rotation: -1});

	// Animation btn_replay
	var tl_btn_replay = new TimelineMax();
	tl_btn_replay.fromTo('.replay_wrapper', 1.5, {left: "110%"}, {left: "80%"}, 4.5);
});