/*=====  ANIMATIONS ECRAN 1  ======*/
$(function() {

	function gotopagetwo(){
		window.location.href = "etape2.html";
	};

	$('.msg').click(function(){
		var tl_boatMove = new TimelineMax({onComplete: gotopagetwo});
		tl_boatMove.to('.bateau', 2, {left: '120%'});
	})

	// Animation du bateau
	var tl_boat = new TimelineMax({repeat: -1});

	tl_boat.fromTo('#scene_ecran1 .bateau',4, {x:'3%', y:0, rotation: -2}, {x:'-3%', y:0, rotation: 2})
	.fromTo('#scene_ecran1 .bateau',4, {x:'-3%', y:0, rotation: 2}, {x:'3%', y:0, rotation: -2});

	// Animation du bateau qui entre en scene
	var tl_boatIn = new TimelineMax({repeat: 0});

	tl_boatIn.fromTo('#scene_ecran1 .bateau', 4, {left:0}, {left:'50%'});

	// Animation des voitures
	var tl_cars = new TimelineMax({repeat: -1});

	tl_cars.fromTo('.camion', 5, {x:-100}, {x:3000})
	.fromTo('.green_car', 5, {x:3000}, {x:-900}, 0)
	.fromTo('.red_car', 5, {x:-600}, {x:3000}, 2)
	.fromTo('.yellow_car', 5, {x:3000}, {x:-600}, 2);

	// Animation du bateau qui quitte la scene
	var tl_boatMove = new TimelineMax({repeat: 0, onComplete: gotopagetwo, paused: true});
	tl_boatMove.to('.bateau', 2, {left: '120%'});

	// Animation btn_suite
	var tl_btn_suite = new TimelineMax({repeat: 0});
	tl_btn_suite.fromTo('#msg_scene_ecran1', 1, {top: "-200%"}, {top: "-85%"}, 2.5)
	.add(tl_boatMove.play(), 8.5);
});